# gitlab-electron

Cross-platform desktop app for GitLab.com

![](https://gitlab.com/greg/gitlab-electron/-/raw/master/gitlab-electron.png)
